


<?php

if($_POST['tile_name'] == "range"){
?>

<div class="modal-content">
    <div class="modal-header">
        <h1 class="modal-title">Tile Range </h1>
        <button type="button" class="modal_close close" >
            <span aria-hidden="true" style="transform: rotate(0deg)">×</span>
        </button>   

    </div>

    <div class="modal-body">
        <div class="card card-primary">
            <div class="card-body">

                <section class="content row spotlight-group " style="margin-top: 0px;">

              

  
    <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">3D PLUS & CARPED </b>
        <a class="spotlight" href="gallery/TILES/3D PLUS & CARPED/3D Ceramic Carpet Tiles With Spanish Beauty Style (20).jpg" >
            <img src="gallery/TILES/3D PLUS & CARPED/3D Ceramic Carpet Tiles With Spanish Beauty Style (20).jpg" alt="3D PLUS & CARPED ">
        </a>
        <a class="spotlight" href="gallery/TILES/3D PLUS & CARPED/maxresdefault.jpg"description="" >
            <img src="gallery/TILES/3D PLUS & CARPED/maxresdefault.jpg" alt="3D PLUS & CARPED ">
        </a>
        
    </p>
    
    <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">FLOOR 2X2</b>
        <a class="spotlight" href="gallery/TILES/FLOOR 2X2/ALMOND BROWN0.jpg" >
            <img src="gallery/TILES/FLOOR 2X2/ALMOND BROWN0.jpg" alt="FLOOR 2X2">
        </a>
        <a class="spotlight" href="gallery/TILES/FLOOR 2X2/3D 1005.jpg"description="" >
            <img src="gallery/TILES/FLOOR 2X2/3D 1005.jpg" alt="FLOOR 2X2">
        </a>
        <a class="spotlight" href="gallery/TILES/FLOOR 2X2/ALMOND BROWN0.jpg" description="" >
            <img src="gallery/TILES/FLOOR 2X2/ALMOND BROWN0.jpg" alt="FLOOR 2X2">
        </a>
    </p>
	  


  <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">FLOOR 4X2</b>
        <a class="spotlight" href="gallery/TILES/FLOOR 4X2/CASTANO BIANCO.jpg" >
            <img src="gallery/TILES/FLOOR 4X2/CASTANO BIANCO.jpg" alt="FLOOR 4X2">
        </a>
        <a class="spotlight" href="gallery/TILES/FLOOR 4X2/CASTANO GRIS.jpg"description="" >
            <img src="gallery/TILES/FLOOR 4X2/CASTANO GRIS.jpg" alt="FLOOR 4X2">
        </a>
        <a class="spotlight" href="gallery/TILES/FLOOR 4X2/LAMBERT.jpg" description="" >
            <img src="gallery/TILES/FLOOR 4X2/LAMBERT.jpg" alt="FLOOR 4X2">
        </a>
    </p>
    
      <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">PANTRY TOP</b>
        <a class="spotlight" href="gallery/TILES/PANTRY TOP/666feefe751ab07883f69aecedf7d239.jpg" >
            <img src="gallery/TILES/PANTRY TOP/666feefe751ab07883f69aecedf7d239.jpg" alt="PANTRY TOP">
        </a>
        <a class="spotlight" href="gallery/TILES/PANTRY TOP/kitchen-tiles-2-1489595333.jpg"description="" >
            <img src="gallery/TILES/PANTRY TOP/kitchen-tiles-2-1489595333.jpg" alt="PANTRY TOP">
        </a>
        
        
    </p>
    
    <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">STEP TILE</b>
        <a class="spotlight" href="gallery/TILES/STEP TILE/4-ft-double-colour-step-riser-tiles-1549347892-4682784.jpeg" >
            <img src="gallery/TILES/STEP TILE/4-ft-double-colour-step-riser-tiles-1549347892-4682784.jpeg" alt="STEP TILE">
        </a>
        <a class="spotlight" href="gallery/TILES/STEP TILE/Building-Material-Marble-Porcelain-Decoration-Step-Tile-Stone-Tile-1000-1200mm-.jpg"description="" >
            <img src="gallery/TILES/STEP TILE/Building-Material-Marble-Porcelain-Decoration-Step-Tile-Stone-Tile-1000-1200mm-.jpg" alt="STEP TILE">
        </a>
   
    </p>
    
     <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">STRIPS</b>
        <a class="spotlight" href="gallery/TILES/STRIPS/Wooden-White-Random-Link2.jpg" >
            <img src="gallery/TILES/STRIPS/Wooden-White-Random-Link2.jpg" alt="STRIPS">
        </a>
        <a class="spotlight" href="gallery/TILES/STRIPS/gold_tile.jpg"description="" >
            <img src="gallery/TILES/STRIPS/gold_tile.jpg" alt="STEP TILE">
        </a>
        
    </p>
    
    <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">TILE BOARDER</b>
        <a class="spotlight" href="gallery/TILES/TILE BOARDER/9620ae5ce21a5a0b8b0fb7aecf11da35.jpg" >
            <img src="gallery/TILES/TILE BOARDER/9620ae5ce21a5a0b8b0fb7aecf11da35.jpg" alt="TILE BOARDER">
        </a>
        <a class="spotlight" href="gallery/TILES/TILE BOARDER/Concordia-White-Polished-Limestone-Tiles-9.jpg" description="" >
            <img src="gallery/TILES/TILE BOARDER/Concordia-White-Polished-Limestone-Tiles-9.jpg" alt="STEP TILE">
        </a>
        <a class="spotlight" href="gallery/TILES/TILE BOARDER/quick-step-livyn-colour-match-skirting-boards-for-pulse-55mm.jpg" description="" >
            <img src="gallery/TILES/TILE BOARDER/quick-step-livyn-colour-match-skirting-boards-for-pulse-55mm.jpg" alt="STEP TILE">
        </a>
        
    </p>

       <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">WALL 12X18 Bathroom</b>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 Bathroom/A1578.jpg" >
            <img src="gallery/TILES/WALL 12X18 Bathroom/A1578.jpg" alt="WALL 12X18 Bathroom">
        </a>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 Bathroom/A1578HL-A.jpg" description="" >
            <img src="gallery/TILES/WALL 12X18 Bathroom/A1578HL-A.jpg" alt="STEP TILE">
        </a>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 Bathroom/Bathroom tiles pictures.jpg" description="" >
            <img src="gallery/TILES/WALL 12X18 Bathroom/Bathroom tiles pictures.jpg" alt="STEP TILE">
        </a>
    </p>
    
      <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">WALL 12X18 KITCHEN</b>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 KITCHEN/20009HL-A.jpg" >
            <img src="gallery/TILES/WALL 12X18 KITCHEN/20009HL-A.jpg" alt="WALL 12X18 KITCHEN">
        </a>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 KITCHEN/20009L.jpg" description="" >
            <img src="gallery/TILES/WALL 12X18 KITCHEN/20009L.jpg" alt="STEP TILE">
        </a>
        <a class="spotlight" href="gallery/TILES/WALL 12X18 KITCHEN/digital-wall-tiles-slider-3.jpg" description="" >
            <img src="gallery/TILES/WALL 12X18 KITCHEN/digital-wall-tiles-slider-3.jpg" alt="STEP TILE">
        </a>
    </p>

                </section>
            </div>

        </div>
        <div class="modal-footer justify-content-between">

            <button type="button" class="btn bg-gradient-warning btn-sm float-leftl modal_close">Close</button>
        </div>
    </div>


</div>

<?php 

}else{
    ?>

<div class="modal-content">
    <div class="modal-header">
        <h1 class="modal-title">Accessories </h1>
        <button type="button" class="modal_close close" >
            <span aria-hidden="true" style="transform: rotate(0deg)">×</span>
        </button>   

    </div>

    <div class="modal-body">
        <div class="card card-primary">
            <div class="card-body">

                <section class="content row spotlight-group " style="margin-top: 0px;">

              

  
    <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">BATH</b>
        <a class="spotlight" href="gallery/ACCESSORIES/BATH/bath-shower-tub-wash-tiles-floor-wall-sri-lanka-1.jpg" >
            <img src="gallery/ACCESSORIES/BATH/bath-shower-tub-wash-tiles-floor-wall-sri-lanka-1.jpg" alt="BATH">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/BATH/the-bathrooms-have-been-updated-but-still-maintain-a-distinct-midcentury-vibe.jpg" description="" >
            <img src="gallery/ACCESSORIES/BATH/the-bathrooms-have-been-updated-but-still-maintain-a-distinct-midcentury-vibe.jpg" alt="BATH">
        </a>
        
    </p>
    
  <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">CABINETS</b>
        <a class="spotlight" href="gallery/ACCESSORIES/CABINETS/01.jpg" >
            <img src="gallery/ACCESSORIES/CABINETS/01.jpg" alt="CABINETS">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/CABINETS/02.jpg" description="" >
            <img src="gallery/ACCESSORIES/CABINETS/02.jpg" alt="CABINETS">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/CABINETS/04.jpg" description="" >
            <img src="gallery/ACCESSORIES/CABINETS/04.jpg" alt="CABINETS">
        </a>
    </p>
    
     <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">CUBICLE</b>
        <a class="spotlight" href="gallery/ACCESSORIES/CUBICLE/11000.jpg" >
            <img src="gallery/ACCESSORIES/CUBICLE/11000.jpg" alt="CUBICLE">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/CUBICLE/80x80-square-acrylic-aluminium-bath-cubicle-shower.jpg" description="" >
            <img src="gallery/ACCESSORIES/CUBICLE/80x80-square-acrylic-aluminium-bath-cubicle-shower.jpg" alt="CUBICLE">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/CUBICLE/Alcove_Cubicle_with_1200mm_Return__59509.1536032703.png" description="" >
            <img src="gallery/ACCESSORIES/CUBICLE/Alcove_Cubicle_with_1200mm_Return__59509.1536032703.png" alt="CUBICLE">
        </a>
    </p>
	  
 <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">FORESETS & ACCESSOIES (ACCURA)</b>
        <a class="spotlight" href="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 01.jpg" >
            <img src="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 01.jpg" alt="FORESETS & ACCESSOIES (ACCURA)">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 04.jpg" description="" >
            <img src="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 04.jpg" alt="FORESETS & ACCESSOIES (ACCURA)">
        </a>
           <a class="spotlight" href="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 03.jpg" description="" >
            <img src="gallery/ACCESSORIES/FORESETS & ACCESSOIES (ACCURA)/Health Faucets 03.jpg" alt="FORESETS & ACCESSOIES (ACCURA)">
        </a>
    </p>

     <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">MIRROR</b>
        <a class="spotlight" href="gallery/ACCESSORIES/MIRROR/A1-Blue-retro-bathroom-mirror-wall-hanging-living-room-sanitary-toilet-makeup-mirror-wx8221506.jpg" >
            <img src="gallery/ACCESSORIES/MIRROR/A1-Blue-retro-bathroom-mirror-wall-hanging-living-room-sanitary-toilet-makeup-mirror-wx8221506.jpg" alt="MIRROR">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/MIRROR/EM1001A.jpg" description="" >
            <img src="gallery/ACCESSORIES/MIRROR/EM1001A.jpg" alt="MIRROR">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/MIRROR/LED33_-_1.jpg" description="" >
            <img src="gallery/ACCESSORIES/MIRROR/LED33_-_1.jpg" alt="MIRROR">
        </a>
    </p>
    
        <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">TOILET SET</b>
        <a class="spotlight" href="gallery/ACCESSORIES/TOILET SET/3195bpzBgVL.jpg" >
            <img src="gallery/ACCESSORIES/TOILET SET/3195bpzBgVL.jpg" alt="TOILET SET">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/TOILET SET/51SNNvki-9L._SL1309_.jpg" description="" >
            <img src="gallery/ACCESSORIES/TOILET SET/51SNNvki-9L._SL1309_.jpg" alt="TOILET SET">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/TOILET SET/HTB1sBpolMLD8KJjSszeq6yGRpXaM.jpg" description="" >
            <img src="gallery/ACCESSORIES/TOILET SET/HTB1sBpolMLD8KJjSszeq6yGRpXaM.jpg" alt="TOILET SET">
        </a>
    </p>
    
     <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">VANITY</b>
        <a class="spotlight" href="gallery/ACCESSORIES/VANITY/6-Ideas-for-a-Budget-Bathroom-Makeover.jpg" >
            <img src="gallery/ACCESSORIES/VANITY/6-Ideas-for-a-Budget-Bathroom-Makeover.jpg" alt="VANITY">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/VANITY/SHELBAS_lrg.jpg" description="" >
            <img src="gallery/ACCESSORIES/VANITY/SHELBAS_lrg.jpg" alt="VANITY">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/VANITY/products_A-SERIES-ROUND-COUNTERTOP-BASIN_ANGLE-768x500.jpg" description="" >
            <img src="gallery/ACCESSORIES/VANITY/products_A-SERIES-ROUND-COUNTERTOP-BASIN_ANGLE-768x500.jpg" alt="VANITY">
        </a>
    </p>
    
       <p class="col-md-6">
        <b style="border-bottom: solid;border-width: 3px;">OTHERS</b>
        <a class="spotlight" href="gallery/ACCESSORIES/18_Border_Tile_bathroom__88941.1437347817.jpg" >
            <img src="gallery/ACCESSORIES/18_Border_Tile_bathroom__88941.1437347817.jpg" alt="">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/2da9059b-33dc-41cf-9f93-1f59d9536cf4-jpg.jpg" >
            <img src="gallery/ACCESSORIES/2da9059b-33dc-41cf-9f93-1f59d9536cf4-jpg.jpg" alt="">
        </a>
        <a class="spotlight" href="gallery/ACCESSORIES/4832_n_PAN-horizon-sky-mosaico-36-kitchen-Copy.jpg" >
            <img src="gallery/ACCESSORIES/4832_n_PAN-horizon-sky-mosaico-36-kitchen-Copy.jpg" alt="">
        </a>
        
        
    </p>
    
                </section>
            </div>

        </div>
        <div class="modal-footer justify-content-between">

            <button type="button" class="btn bg-gradient-warning btn-sm float-leftl modal_close">Close</button>
        </div>
    </div>


</div>


    <?php
}
?>