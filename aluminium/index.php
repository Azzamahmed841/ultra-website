<?php
//active link

$Home = 'active';

//active link end
//page name

$main_page = "Home";
$Page_name = "Dashboard";

$brand = "menu-open active";
$aluminium_ = "active";
$elevation_aluminium = "brand_logo_active";

//end heare
?>

<style>
    #aluminium{
          -webkit-animation:spin 4s linear infinite;
    -moz-animation:spin 4s linear infinite;
    animation:spin 4s linear infinite;
    }
    @-moz-keyframes spin { 100% { transform: rotate(360deg); } }
@-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
@keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); }}
    </style>
<?php
include '../theme/header.php';
?>



<div class="modal fade" id="modal-lg" style="display: none;" aria-hidden="true" >
        <div class="modal-dialog modal-lg" >
          <div class="modal-content" style="width: 100%;height: 100%">
            <div class="modal-header">
             
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="transform: rotate(0deg)">×</span>
              </button>
            </div>
            <div class="modal-body">
             <img class="imgx" src="" style="width: 100%;height: 100%;">
            </div>
            <div class="modal-footer justify-content-between">
             
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>


<section class="content" >
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        
                        <div class="images"> <!-- the image container --> 
         <img  src="../theme/src/images/ultra_aluminum/ultra_main.jpg" style="width: 100%;height: 500px;">
               <!-- the image --> 
               <center><h1 id="h1">ULTRA &nbsp; ALUMINIUM</h1></center><!-- the text --> 
</div> 
                        
                    </div>
                    <div class="card-body row">

                        <h1>Welcome to Ultra Aluminium</h1>
                        
                        <p>We are specialized in manufacturing, Importing and supplying superior quality 
                            Aluminium Extrusions. we have been proudly supplying the high standard exclusive
                            ranges of products all over the island over the years.</p>

                    </div>

                </div>
            </div>
            <style>
                .imgs:hover{
                    cursor: zoom-in;
                }
                </style>
                 <div class="col-md-4">
                <div class="card card-outline card-info">
                   
                    <div class="card-body row">
                        <img class="imgs"  src="../theme/src/images/ultra_aluminum/Standard for Base Materials.jpg" style="width: 100%;height: 400px;">
                    </div>

                </div>
            </div>
                 <div class="col-md-4">
                <div class="card card-outline card-primary">
                   
                    <div class="card-body row">
                        <img class="imgs" src="../theme/src/images/ultra_aluminum/Standard for Anodic Oxidation Profiles.jpg" style="width: 100%;height: 400px;">
                    </div>

                </div>
            </div>
                 <div class="col-md-4">
                <div class="card card-outline card-warning">
                   
                    <div class="card-body row">
                        <img class="imgs" src="../theme/src/images/ultra_aluminum/Standard for Powder Spraying Profiles.jpg" style="width: 100%;height: 400px;">
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->

</section>
<!-- /.content -->

<!-- /.content-wrapper -->



<script>
    var load_theme = 0;

</script>

<?php include '../theme/footer.php'; ?>

<script>



    $(document).ready(function () {


$(".imgs").click(function(){

   $(".modal").modal('show');
  $(".imgx").attr("src", $(this).attr("src"));
});

    });
</script>