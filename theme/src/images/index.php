<?php

session_start();
if (!isset($_SESSION['uid'])) {
    header("location:../index.php");
    exit;
} else {
     header("location:../views/main/home_page.php");
}   
