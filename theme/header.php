


<?php
session_start();

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
?>




<!DOCTYPE html>
<html lang="en" >
    <head>
        <link rel="icon" type="image/png" href="../../images/Altec_image.png">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Ultra Holdings Lanka (PVT) LTD </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="../theme/src/plugins/fontawesome-free/css/all.min.css">
        <!-- IonIcons -->
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../theme/src/dist/css/adminlte.min.css">
        <link rel="stylesheet" href="../theme/src/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="../theme/src/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">




        <link rel="stylesheet" href="../theme/src/print/css/semantic.min.css">
        <link rel="stylesheet" href="../theme/src/print/css/dataTables.semanticui.min.css">
        <link rel="stylesheet" href="../theme/src/print/css/buttons.semanticui.min.css">

        <!-- daterange picker -->
        <link rel="stylesheet" href="../theme/src/plugins/daterangepicker/daterangepicker.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="../theme/src/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="../theme/src/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="../theme/src/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="../theme/src/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="../theme/src/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="../theme/src/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- Toastr -->
        <link rel="stylesheet" href="../theme/src/plugins/toastr/toastr.min.css">
        <link rel="stylesheet" href="../theme/src/css/PurchaseStyle.css">
        <link rel="stylesheet" href="../theme/src/css/style.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- jQuery -->
        <script src="../theme/src/plugins/jquery/jquery.min.js"></script>


        <style>

            .images  
            {  
                position: relative; /* To help the image + text element to get along with the rest of the page*/  
                width: 100%; /* for IE 6 */  
            }  
            #h1  
            {  
                position: absolute; /* To place the text on the image*/ 
                top: 0px; /* The exact location of the text from the top of the image*/ 
                left: 0; /* Other beautification stuff */ 
                width: 100%; 
                font-size: 360%;

            } 
            /* Coloring time */ 
            #h1 span /* decorating the text within the span tag */ 
            {  
                color: white;  
                font: bold 24px/45px Helvetica, Sans-Serif;  
                letter-spacing: -1px;  
                background: rgb(0, 0, 0); /* fallback color */  
                background: rgba(0, 0, 0, 0.7); padding: 10px;  
            } 


        </style>

        <style>


            .no-js #loader { display: none;  }
            .js #loader { display: block; position: absolute; left: 100px; top: 0; }
            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('../theme/src/images/Altec-Preloader.gif') center no-repeat #fff;
            }

            .se-pre-con_ite {
                position: fixed;
                /*                left: 30%;*/
                top: 50%;
                width: 50%;
                height: 50%;
                z-index: 9999;
                background: url('../theme/src/images/Altec-Preloader.gif') center no-repeat #f8f8f800;
            }

            .outer {
                display: table;
                position: absolute;
                top: -100px;
                left: 0;
                height: 100%;
                width: 100%;
            }

            .middle {
                display: table-cell;
                vertical-align: middle;
            }

            .inner {
                margin-left: auto;
                margin-right: auto;
                width: 400px;
                /*whatever width you want*/
            }



            @media (min-width: 992px){

                .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .main-footer, .sidebar-mini.sidebar-collapse  {
                    margin-left: 4.6rem!important;
                }
                .main-header {
                    margin-left: 0rem!important;
                }
                .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .main-footer, .sidebar-mini .sidebar-collapse {
                    margin-left: 4.6rem!important;
                }
                .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .main-footer, .sidebar-mini.sidebar-collapse .main-header {
                    margin-left: 0rem!important;
                }
            }

            .layout-navbar-fixed.layout-fixed .wrapper .sidebar {
                margin-top: calc(-3rem);
            }

            @media screen and (min-width: 992px) {
                body{
                    zoom: 70%;
                    font-size: 150%;
                }
                #hom_img{
                    width: 250px;
                }
                #bun { 

                    font-size: 150%;
                }  /* show it on small screens */
                #head_logo{
                    margin: auto;
                    height: 100px;

                }
                .layout-fixed .main-sidebar {
                    bottom: 0;
                    float: none;
                    height: 100vh;
                    left: 0;
                    position: fixed;
                    top: 115px;
                    width: 500px;
                }
                .content{
                    margin-top: 130px;
                }
                .brand_logo{

                    height: 100px;
                    width: 100px;
                    padding: 5px;

                }
                .a_brand{
                    margin-left: 200px;
                }

            }

            @media screen and  (max-width: 992px) {

                #bun { 


                    font-size: 150%;

                }   /* hide it elsewhere */
                #hom_img{
                    display: none;
                }
                #head_logo{
                    margin: auto;
                    height: 50px;
                }
                .layout-fixed .main-sidebar {
                    bottom: 0;
                    float: none;
                    height: 100vh;
                    left: 0;
                    position: fixed;
                    top: 65px;
                }
                .content{
                    margin-top: 80px;
                }
                .brand_logo{
                    height: 100%;
                    width: 50%;
                    padding: 5px;


                }
                .a_brand{
                    margin-left: 0%;
                }
                .info{
                    margin-top: 20px;

                }
            }

            .layout-fixed .wrapper .sidebar {
                height: 90%;
            }

            @media (min-width: 768px){
                body:not(.sidebar-mini-md) .content-wrapper, body:not(.sidebar-mini-md) .main-footer, body:not(.sidebar-mini-md) .main-header {
                    transition: margin-left .3s ease-in-out;
                    margin-left: 500px;
                }

                #bun { 



                    display: none;
                }
                body{
                    font-size: 25px;
                }
            }


            .navbar-light {
                background-color: #94999f;
            }
            
            .content-wrapper{
                background: none;
            }
            body{
                background-image: url("../theme/src/images/ultra_bag.jpg");
            }
            .card{
                background: none;
            }
            
            
        </style>



<!--        <script type="text/javascript">
            window.onload = setTimeout(function () {
                $(".se-pre-con").fadeOut("slow");
            }, 1000);
            $(window).load(function () {

                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");
            });
        </script>-->
        <script>
            $(document).ready(function () {


                $(".tiles").hover(function () {
                    $(".tiles").toggleClass('st_active');
                })
                $(".roof").hover(function () {
                    $(".roof").toggleClass('st_active');
                })
                $(".aluminium").hover(function () {
                    $(".aluminium").toggleClass('st_active');
                })
                $(".steel").hover(function () {
                    $(".steel").toggleClass('st_active');
                })

            });
        </script>
    </head>
    <!--layout-footer-fixed-->
    <body class="sidebar-mini  layout-navbar-fixed   layout-fixed" >



        <!--        <div class="se-pre-con_ite d-none" id="loadingDiv" >  
                    <div class="outer">
                        <div class="middle">
                            <div class="inner">
                                <center><img style="vertical-align: middle;" src="../theme/src/images/ALTEC-LOGO-FINAl-Red-and-black-webformet.png" /><br> <br>    
                                    <h2> Loading please wait<h2> </center><br>
        
                                            </div>
                                            </div>
                                            </div>
                                            </div>-->
        <!-- Loading Gif-->

        <!--
                                            <div class="se-pre-con">
                                                <div class="outer">
                                                    <div class="middle">
                                                        <div class="inner">
                                                            <center><img style="vertical-align: middle;" src="../theme/src/images/ALTEC-LOGO-FINAl-Red-and-black-webformet.png" /><br> <br>
                                                                <h2>Loading please wait<h2></center><br>
        
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>-->

        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-light " >
                <!-- Left navbar links -->

                <a href="../ultra/"  > <img src="../theme/src/images/Ultra.gif"  id="hom_img"> </a>
                <ul class="navbar-nav " id="bun">
                    <li class="nav-item">
                        <a  class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>


                </ul>
                <!-- SEARCH FORM -->
                <!--form class="form-inline ml-3">
                  <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </form-->

                <!-- Right navbar links -->

                <ul class="navbar-nav" id="head_logo">

                    <style>

                        .bran_logo{
                            height: 100px;
                            width: 100px;
                            margin-right: 130px;
                            padding: 5px;
                        }



                        .brand_logo:hover {
                            box-shadow: 0 19px 38px rgba(0,0,0,.5),0 15px 12px rgba(0,1,121,.50)!important;
                            transition: 0.2s;
                            opacity: 1; 
                            filter: alpha(opacity=100);
                            position:relative;
                            -webkit-transform:scale(1.1); /* Safari and Chrome */
                            -moz-transform:scale(1.1); /* Firefox */
                            -ms-transform:scale(1.1); /* IE 9 */
                            -o-transform:scale(1.1); /* Opera */
                            transform:scale(1.1);
                            cursor: pointer !important;
                            color: #fff;
                            background-color: #0069d9;
                            border-color: #0062cc;
                        }


                        .brand_logo_active {
                            box-shadow: 0 19px 38px rgba(0,0,0,.5),0 15px 12px rgba(0,1,121,.50)!important;
                            transition: 0.2s;
                            opacity: 1; 
                            filter: alpha(opacity=100);
                            position:relative;
                            /*                            -webkit-transform:scale(1.1);  Safari and Chrome 
                                                        -moz-transform:scale(1.1);  Firefox 
                                                        -ms-transform:scale(1.1);  IE 9 
                                                        -o-transform:scale(1.1);  Opera 
                                                        transform:scale(1.1);*/
                            cursor: pointer !important;
                            color: #fff;
                            background-color: #0069d9;
                            border-color: #0062cc;
                        }

                    </style>

                    <a href="../tiles/" class="a_brand"> <img src="../theme/src/images/ultra_tiles/ultra_tiles.jpeg" class="brand_logo img-circle elevation-1 <?= $elevation_tiles ?> tiles" > </a>
                    <a href="../roof/"  class="a_brand"> <img src="../theme/src/images/ultra_roof/ultra_roof.jpeg" class="brand_logo img-circle elevation-1 <?= $elevation_roof ?> roof" > </a>
                    <a href="../aluminium/"  class="a_brand"> <img src="../theme/src/images/ultra_aluminum/ultra_aluminum.jpeg" class="brand_logo img-circle elevation-1 <?= $elevation_aluminium ?> aluminium" > </a>
                    <a href="../steel/"  class="a_brand"> <img src="../theme/src/images/ultra_steel/ultra_steel.jpeg" class="brand_logo img-circle elevation-1 <?= $elevation_steel ?> steel" > </a>


                </ul>

            </nav>
            <!-- /.navbar -->

            <?php include 'side_bar.php'; ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
