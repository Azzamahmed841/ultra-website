
<!--   Control Sidebar 
  <aside class="control-sidebar control-sidebar-dark">
     Control sidebar content goes here 
  </aside>
   /.control-sidebar 

   Main Footer 
  <footer class="main-footer">
   <strong>   Copyright © 2020 All rights reserved by <a href="https://www.altec.lk"> Altec IT Solutions(PVT) LTD.</a>  Proudly developed by Altec IT Solutions Software Engineers Team.</strong> 
 
    <div class="float-right d-none d-sm-inline-block">
      <b> Version <?= ucfirst($_SESSION['version']) ?></b>
    </div>
  </footer>-->

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
</div>
<!-- jQuery -->
<script src="../theme/src/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../theme/src/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../theme/src/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../theme/src/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../theme/src/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../theme/src/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>



<script src="../theme/src/print/js/dataTables.semanticui.min.js"></script>
<script src="../theme/src/print/js/dataTables.buttons.min.js"></script>
<script src="../theme/src/print/js/buttons.semanticui.min.js"></script>
<script src="../theme/src/print/js/jszip.min.js"></script>
<script src="../theme/src/print/js/pdfmake.min.js"></script>
<script src="../theme/src/print/js/vfs_fonts.js"></script>
<script src="../theme/src/print/js/buttons.html5.min.js"></script>
<script src="../theme/src/print/js/buttons.print.min.js"></script>
<script src="../theme/src/print/js/buttons.colVis.min.js"></script>

<!-- Select2 -->
<script src="../theme/src/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../theme/src/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../theme/src/plugins/moment/moment.min.js"></script>
<script src="../theme/src/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="../theme/src/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../theme/src/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../theme/src/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="../theme/src/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>


<!-- SweetAlert2 -->
<script src="../theme/src/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="../theme/src/plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE -->
<script src="../theme/src/dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="../theme/src/plugins/chart.js/Chart.min.js"></script>
<script src="../theme/src/dist/js/demo.js"></script>
<!--<script src="../theme/src/dist/js/pages/dashboard3.js"></script>-->
</body>
</html>


<script>

    $(document).ready(function () {
        var options = {
            speed: 1000,

        }

        $('.content').hide();

        $('.content').show(options.speed);



        $('.select2').select2({
            theme: 'bootstrap4'
        });



        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment(),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'))
                    $('#rang_val').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'))
                }
        )


    });

</script>