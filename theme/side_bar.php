<!-- Main Sidebar Container -->

<style>

    [class*=sidebar-light-] {
        background-color: #94999f;
    }
    
    
/*[class*=sidebar-dark-] .nav-sidebar>.nav-item.menu-open>.nav-link, [class*=sidebar-dark-] .nav-sidebar>.nav-item:hover>.nav-link, [class*=sidebar-dark-] .nav-sidebar>.nav-item>.nav-link:focus {
    background-color: rgba(255,255,255,.1);
    color: #000;
}*/


/*.nav-link p, .nav-link i{
    color:#000;
    
}
.nav-link p active{
    color:#fff!important;
}*/
</style>
<aside class="main-sidebar elevation-4 sidebar-light-cyan">
    <!-- Brand Logo -->
    <!--   
        <a class="brand-link" style="height: 115px;">
        
            <span class="brand-text font-weight-light">
                <img src="../theme/src/images/Ultra.gif"    style="opacity: .8;width: 250px;height: 100px;padding: 10px;">
            </span>
        </a>-->
    <style>
        .user-panel img {
            height: 150px;
            width: 150px;
        }
    </style>

    <style>
        .st{

            background: #85C1E9;
            height: 100px;
            width: 100px;
            margin-right: -5px;
            font-weight: bold;
            color: #001f3f;
            padding: 0px;
            margin-top: -2px;
            border: solid;
            font-size: 18px;



        }

         @media screen and  (max-width: 992px) {
         
              .st{

        
            height: 75px;
            width: 75px;
         
                        font-size: 15px;
               
    



        }
        
        #rotate{
            margin-left: -22px;
        }
         }
         
        .st:hover{
            box-shadow: 0 19px 38px rgba(0,0,0,.5),0 15px 12px rgba(0,1,121,.50)!important;
            transition: 0.2s;
            opacity: 1; 
            filter: alpha(opacity=100);
            position:relative;
            -webkit-transform:scale(1.1); /* Safari and Chrome */
            -moz-transform:scale(1.1); /* Firefox */
            -ms-transform:scale(1.1); /* IE 9 */
            -o-transform:scale(1.1); /* Opera */
            transform:scale(1.1);
            cursor: pointer !important;
        }
        .st_active{
            box-shadow: 0 19px 38px rgba(0,0,0,.5),0 15px 12px rgba(0,1,121,.50)!important;
            transition: 0.2s;
            opacity: 1; 
            filter: alpha(opacity=100);
            position:relative;
            -webkit-transform:scale(1.1); /* Safari and Chrome */
            -moz-transform:scale(1.1); /* Firefox */
            -ms-transform:scale(1.1); /* IE 9 */
            -o-transform:scale(1.1); /* Opera */
            transform:scale(1.1);
            cursor: pointer !important;
            color: #fff;
            background-color: #0069d9;
            border-color: #0062cc;
        }

        #rotate{
            transform: rotate(46deg);
            display: inline-block;
        }
        button span {
            display: inline-block;
            transform: rotate(-46deg);
        }
    </style>

    <!-- Sidebar -->
    <div class="sidebar ">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex" style="height: 380px;padding-top: 30px;">


            <div class="info" style="padding: 60px;margin: auto;">
                <span id="rotate">
                    <a href="../tiles/"><button id="tiles" class="st btn btn-primary tiles <?= $elevation_tiles." ".$tiles_?> "><span id="tiles_span">Tiles</span></button></a>
                    <a href="../aluminium/"><button id="aluminium" class="<?= $elevation_aluminium." ".$aluminium_ ?> st btn btn-primary aluminium"><span>Aluminium</span></button></a><br>
                    <a href="../roof/"><button id="roof" class="st btn btn-primary roof <?= $elevation_roof." ".$roof_ ?>"><span>Roof</span></button> </a>
                    <a href="../steel/"><button id="steel" class="<?= $elevation_steel." ".$steel_ ?> st btn btn-primary steel"><span>Steel</span></button></a>
                </span>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="../who-we-are/" class="nav-link <?= $who_we_are ?>">
                        <i class="fas fa-users nav-icon"></i>
                        <p>
                            Who We Are
                           
                        </p>
                    </a>

                </li>


                <li class="nav-item has-treeview <?= $brand ?>">
                    <a href="#" class="nav-link <?= $brand ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Our Brand
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="../tiles/" class="nav-link <?= $tiles_ ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ultra Tiles</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../roof/" class="nav-link <?= $roof_ ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ultra Roof</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../aluminium/" class="nav-link <?= $aluminium_ ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ultra Aluminium</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="../steel" class="nav-link <?= $steel_ ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ultra Steel</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="../news/" class="nav-link <?=$news?>">
                        <i class="fas fa-newspaper nav-icon"></i>
                        <p>
                            News Line.
                           
                        </p>
                    </a>

                </li>

                <li class="nav-item has-treeview">
                    <a href="../career/" class="nav-link <?= $career ?>">
                        <i class="fas fa-hands-helping nav-icon"></i>
                        <p>
                            Career
                         
                        </p>
                    </a>

                </li>

                <li class="nav-item has-treeview">
                    <a href="../contect/" class="nav-link <?=$contact?>">
                        <i class="fas fa-link nav-icon"></i>
                        <p>
                            Contact
                          
                        </p>
                    </a>

                </li>



            </ul>
<hr>

            <ul  class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                <center>
                    <a href="https://facebook.com" target="_blank" style="margin: 15px;">
                        <i class="fab fa-facebook-f"></i>

                    </a>
                    <a href="https://twitter.com" target="_blank" style="margin: 15px;">
                      <i class="fab fa-twitter"></i>

                    </a>
                    <a href="https://twitter.com" target="_blank" style="margin: 15px;">
                      <i class="fab fa-linkedin-in"></i>

                    </a>
                </center>
                </li>
            </ul>


        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>